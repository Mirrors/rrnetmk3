memcheck62.prg is a small test program that loads on C64 to $0801-$ffff and
then validates the loaded data and prints non-matching memory addresses.

Binary only, no source code, written using a direct assembler. 

this can be used to check if file uploads work - upload and run via
- codenet
- udpsend
- tftp
