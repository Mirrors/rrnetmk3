
; (c)opyright 2014-2017
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

.include "rrnetmk3.inc"

.segment "CODE"

handle_echo_package:
   lda   IP_PACKAGE_UDP_SRC_PORT+0
   sta   IP_PACKAGE_UDP_DEST_PORT+0
   lda   IP_PACKAGE_UDP_SRC_PORT+1
   sta   IP_PACKAGE_UDP_DEST_PORT+1
   lda   #>ECHO_PORT
   sta   IP_PACKAGE_UDP_SRC_PORT+0
   lda   #<ECHO_PORT
   sta   IP_PACKAGE_UDP_SRC_PORT+1

   jsr   package_ip_reply
   jsr   area_set_start
; calculate a correct end of buffer
; the hardware will pad packages with < 60 bytes, on BOTH reading and writing
   lda   #<(IP_PACKAGE_UDP_START)
   clc
   adc   IP_PACKAGE_UDP_LENGTH+1
   sta   AREA_END+0
   lda   #>(IP_PACKAGE_UDP_START)
   adc   IP_PACKAGE_UDP_LENGTH+0
   sta   AREA_END+1

   jsr   udp_send_package
   jmp   receive_loop   
