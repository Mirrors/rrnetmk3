
; (c)opyright 2014-2017
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

.include "rrnetmk3.inc"

IP_PACKAGE_CODENET_HEADER        := IP_PACKAGE_UDP_PAYLOAD + $00
IP_PACKAGE_CODENET_SEQUENCE      := IP_PACKAGE_UDP_PAYLOAD + $02
IP_PACKAGE_CODENET_SERVICE       := IP_PACKAGE_UDP_PAYLOAD + $03
IP_PACKAGE_CODENET_ADDRESS       := IP_PACKAGE_UDP_PAYLOAD + $04
IP_PACKAGE_CODENET_SIZE          := IP_PACKAGE_UDP_PAYLOAD + $06
IP_PACKAGE_CODENET_DATA          := IP_PACKAGE_UDP_PAYLOAD + $08

.segment "CODE"

back_to_loop:
   jmp   receive_loop

handle_udp_codenet:
   ; check if mode is unset
   lda   TRANSFER_MODE
   bne   @check_mode_codenet
   ; yes, set mode to codenet
   jsr   save_remote_ip_and_mac
   lda   #TRANSFER_MODE_CODENET
   sta   TRANSFER_MODE
@check_mode_codenet:
   ; check if mode is codenet
   cmp   #TRANSFER_MODE_CODENET
   bne   back_to_loop

   lda   IP_PACKAGE_CODENET_HEADER+0
   cmp   #>CODENET_MAGIC
   bne   back_to_loop
   lda   IP_PACKAGE_CODENET_HEADER+1
   cmp   #<CODENET_MAGIC
   bne   back_to_loop
   lda   IP_PACKAGE_CODENET_SERVICE       ; get service id
   sta   CODENET_SERVICE_ID               ; store it on the stack
   lda   #CODENET_SERVICE_ACK
   sta   IP_PACKAGE_CODENET_SERVICE
   lda   #$00
   sta   IP_HEADER_TOTAL_LEN+0
   sta   IP_PACKAGE_UDP_LENGTH+0
   lda   #$20
   sta   IP_HEADER_TOTAL_LEN+1
   lda   #$0c
   sta   IP_PACKAGE_UDP_LENGTH+1

   lda   IP_PACKAGE_UDP_SRC_PORT+0
   sta   PORT_REMOTE+0
   lda   IP_PACKAGE_UDP_SRC_PORT+1
   sta   PORT_REMOTE+1
.if 1
   jsr   recall_cancel
.else
; do NOT use this: codenet does not expect resending lost packages
@resend_codenet_ack:
   lda   #<@resend_codenet_ack
   ldy   #>@resend_codenet_ack
   jsr   recall_after_2s
.endif

; switch source and destination ports
   lda   PORT_REMOTE+0
   sta   IP_PACKAGE_UDP_DEST_PORT+0
   lda   PORT_REMOTE+1
   sta   IP_PACKAGE_UDP_DEST_PORT+1
   lda   #>CODENET_PORT
   sta   IP_PACKAGE_UDP_SRC_PORT+0
   lda   #<CODENET_PORT
   sta   IP_PACKAGE_UDP_SRC_PORT+1

   lda   #<IP_PACKAGE_CODENET_ADDRESS
   sta   AREA_END+0
   lda   #>IP_PACKAGE_CODENET_ADDRESS
   sta   AREA_END+1
   ;jsr   udp_set_package_sizes
   jsr   set_udp_checksum

   jsr   transfer_ip_reply
   jsr   codenet_send_ack
   lda   IP_PACKAGE_CODENET_ADDRESS+0
   sta   TEMP_B_16+1
   lda   IP_PACKAGE_CODENET_ADDRESS+1
   sta   TEMP_B_16+0
   jsr   current_address_out
   ldx   IP_PACKAGE_CODENET_SIZE+0
   ldy   IP_PACKAGE_CODENET_SIZE+1
   lda   CODENET_SERVICE_ID
   cmp   #CODENET_SERVICE_DATA
   beq   codenet_svc_data
   cmp   #CODENET_SERVICE_FILL
   beq   codenet_svc_fill
   cmp   #CODENET_SERVICE_JUMP
   beq   codenet_svc_jump
   cmp   #CODENET_SERVICE_RUN
   beq   codenet_svc_run
@exit:
   jmp   receive_loop
codenet_svc_run:
   jmp   disable_and_run
codenet_svc_jump:
   jmp   disable_and_jump

codenet_svc_data:
   sei
   lda   #$33
   sta   $01
   ldx   IP_PACKAGE_CODENET_SIZE+$01
   ldy   #$00
@copyloop:
   lda   IP_PACKAGE_CODENET_DATA,y

   sta   (TEMP_B_16),y
   iny
   dex
   bne   @copyloop
   clc
   tya
   adc   TEMP_B_16+0
   sta   LOAD_VEC+0
   lda   TEMP_B_16+1
   adc   #$00
   sta   LOAD_VEC+1
   lda   #$37
   sta   $01
   cli
   jmp   receive_loop

codenet_svc_fill:
   clc
   tya
   adc   TEMP_B_16+0
   sta   TEMP_A_16+0
   txa
   adc   TEMP_B_16+1
   sta   TEMP_A_16+1
   sei
   lda   #$33
   sta   $01
   ldy   TEMP_B_16+0
   lda   #$00
   sta   TEMP_B_16+0
   lda   IP_PACKAGE_CODENET_DATA; fill byte
@fillloop:
   sta   (TEMP_B_16),y
   iny
   bne   :+
   inc   TEMP_B_16+1
:
   cpy   TEMP_A_16+0
   bne   @fillloop
   ldx   TEMP_B_16+1
   cpx   TEMP_A_16+1
   bne   @fillloop
   lda   #$37     ; return access to io
   sta   $01
   cli
   jmp   receive_loop

codenet_send_ack:
   clc
   lda   IP_HEADER_TOTAL_LEN+$01
   adc   #$0E
   tax
   lda   IP_HEADER_TOTAL_LEN+$00
   adc   #$00
.if 0
   bne   :+
   ; todo check maximum allowed buffer size
   tay
   jmp   cs8900a_send_ip_frame_16
:
   rts
.else
   tay
   jmp   cs8900a_send_ip_frame_16
.endif
