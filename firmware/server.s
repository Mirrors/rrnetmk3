
; (c)opyright 2014-2017
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

.include "rrnetmk3.inc"

.segment "CODE"

run_server_enter_ip:
   jsr   enter_ip_address
run_server_static_ip:
   lda   #DHCP_STATE_OFF
   .byte $2c
run_server_dhcp:
   lda   #DHCP_STATE_DISCOVER
   sta   DHCP_STATE
restart_server:
   jsr   print
   .byte $93,$02,$11,$01,$05,"rr-nET RUNNING IN SERVER MODE",$9A,$0D
   .byte " pRESS ",$12," run/stop ",$92," TO ABORT",$1D,$1D,$1D
   .byte "cHIP-rEV:?",$0D
   .byte $02,$13,$01,"lINK  tx:00 rx:00 dr:00"
.if USE_DEBUG
   .byte $02,$15,$01,"RXE:ERCBIOD BST:RT LST:CPT"
.endif
   .byte $00
   lda   CS8900A_REVISION ; these are logically ordered as B, C, D, F
   cmp   #$03
   adc   #$42
   sta   $0400 + $28 * 18 + 38 ; $06f6
   jsr   cs8900a_print_tx
   jsr   cs8900a_print_rx
   jsr   cs8900a_print_dropped

   lda   DHCP_STATE
   beq   :++
   lda   #$00
   ldx   #$03
:
   sta   RAM_IP_ADDRESS,x
   dex
   bpl   :-
:
   lda   DHCP_STATE
   jsr   print_hw_info
   jsr   print_dhcp_state

   ldx   #<ROM_MAC_ADDR
   ldy   #>ROM_MAC_ADDR
   jsr   cs8900a_setup_mac
   bpl   receive_start
; TODO: display error message
   jmp   disable_and_basic

receive_start:
   jsr   clear_netbuffer
receive_loop:
.if 0
; debug code: copy value of TI to screen memory
   ldx   #$02
:
   lda   TI,x
   sta   $0680,x
   dex
   bpl   :-
.endif
;   sei
   jsr   check_keys_pressed
   bne   :+
   jmp   disable_and_basic
:
   jsr   print_status
   bpl   receive_loop   ; no link

   lda   TIMESTAMP_RECALL
   beq   @no_resend
; check vs kernal: TI
.if 0
   lda   TI+0
   bne   @go_resend
.endif
   ; trying to get the desired 8-bit window out of the 24-bit value "TI"
   lda   TI+2
   asl
   lda   TI+1
   asl
   cmp   TIMESTAMP_RECALL
   bcc   @no_resend
@go_resend:
   lda   FUNCTION_RECALL+1
   pha
   lda   FUNCTION_RECALL+0
   pha
   lda   #$00
   sta   TIMESTAMP_RECALL
   php                     ; using rti instead of rts to circumvent the
   rti                     ; return address minus one topic
@no_resend:

   lda   DHCP_STATE
   cmp   #DHCP_STATE_DISCOVER
   bne   :+
   jsr   print_dhcp_state
   lda   #DHCP_STATE_OFFER
   jsr   set_dhcp_state
   jmp   dhcp_create_package_discover
:
   jsr   cs8900a_receive_ip_frame_16
   bmi   receive_loop
; check for protocol type IP (big endian: $0800)
   lda   ETHER_FRAME_TYPE+0
   cmp   #$08
   bne   receive_loop
   lda   ETHER_FRAME_TYPE+1
   beq   @handle_ip
   cmp   #$06 ; ARP (big endian: $0806)
   bne   receive_loop
   jsr   reply_arp
   jmp   receive_loop

@handle_ip:
   lda   IP_HEADER_HEADER_LEN+$00
   cmp   #$45
   bne   @receive_loop
   lda   IP_HEADER_FLAGS
   and   #$BF
   ora   IP_HEADER_FRAG_OFFSET
   bne   @receive_loop
   lda   IP_HEADER_PROTOCOL
   cmp   #PROTOCOL_UDP
   beq   handle_udp_package
   cmp   #PROTOCOL_ICMP
   beq   handle_icmp_package
@receive_loop:
   jmp   receive_loop

handle_icmp_package:
   lda   IP_PACKAGE_ICMP_TYPE+$00
   cmp   #$08     ; check for echo request
   bne   back_to_loop
   lda   #$00     ; set to echo reply
   sta   IP_PACKAGE_ICMP_TYPE+$00
   clc
   lda   IP_PACKAGE_ICMP_CHECKSUM+$00
   adc   #$08
   sta   IP_PACKAGE_ICMP_CHECKSUM+$00
   bcc   :++
:
   inc   IP_PACKAGE_ICMP_CHECKSUM+$01
   bne   :+
   inc   IP_PACKAGE_ICMP_CHECKSUM+$00
   beq   :-
:
   jsr   package_ip_reply
   jsr   codenet_send_ack
back_to_loop:
   jmp   receive_loop

handle_udp_package:
   lda   IP_PACKAGE_UDP_DEST_PORT+1
   ldx   IP_PACKAGE_UDP_DEST_PORT+0
   bne   check_16bit_ports

; 8-bit ports
check_udp_echo:
   cmp   #ECHO_PORT
   bne   check_udp_dhcp
   jmp   handle_echo_package

check_udp_dhcp:
   cmp   #BOOTPC_PORT
   bne   check_tftp_server_port
   jmp   handle_dhcp_package

check_tftp_server_port:
   cmp   #TFTP_SERVER_PORT
   bne   check_tftp_data_port
   jmp   handle_tftp_command
check_tftp_data_port:
   cmp   #TFTP_TRANSFER_PORT
   bne   unhandled_udp_package
   jmp   handle_tftp_data

; 16-bit ports
check_16bit_ports:
check_sendudp_port:
   cpx   #>SENDUDP_PORT
   bne   check_codenet_port
   cmp   #<SENDUDP_PORT
   bne   check_codenet_port
   jmp   handle_udp_sendudp

check_codenet_port:
   cpx   #>CODENET_PORT
   bne   unhandled_udp_package
   cmp   #<CODENET_PORT
   bne   unhandled_udp_package
   jmp   handle_udp_codenet

unhandled_udp_package:
.if ENABLE_IGMP
   lda   #$03
   jsr   handle_igmp_package
.endif
   jmp   receive_loop

; both routines take a/y (lo/hi) as the routine to jump to
recall_after_2m:
   ldx   #$71
   .byte $2c
recall_after_2s:
   ldx   #$01
   sei
   stx   TIMESTAMP_RECALL
   ldx   #$00
   stx   TI+1
   stx   TI+2
   sta   FUNCTION_RECALL+0
   sty   FUNCTION_RECALL+1
   cli

   rts

recall_cancel:
   lda   #$00
   sta   TIMESTAMP_RECALL
   rts

.if 0
cleartod:
   ldy   #$03
   lda   #$00
:
   sta   CIA1_BASE+CIA_TOD_10TH,y
   dey
   bpl   :-
   rts
.else
cleartime:
   lda   #$00
   sta   $a1
   sta   $a2
   rts
.endif

