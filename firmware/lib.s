
; (c)opyright 2014-2017
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

.include "rrnetmk3.inc"


.segment "JMPTABLE"
; jumptable for externally usable subroutines
; can be called using
; lda #$00
; sta $de00
; sta $de80
; jmp jt_*
; this will trigger both possible bankswitching schemes
; placeholder for jump $9FD0
   .byte $00,$00,$00
; placeholder for jump $9FD3
   .byte $00,$00,$00
; placeholder for jump $9FD6
   .byte $00,$00,$00
; placeholder for jump $9FD9
   .byte $00,$00,$00
; placeholder for jump $9FDC
   .byte $00,$00,$00
; placeholder for jump $9FDF
   .byte $00,$00,$00
; placeholder for jump $9FE2
   .byte $00,$00,$00

jt_print_hexbyte:
; $9FE5: prints out hex value from "00"-"FF"
; note: routine ends with "SEI"
; A contains value
   jmp   hexbyte_out

jt_convertascii:
; $9FE8: converts ascii to petscii text by converting [A-Z] <-> [a-z]
; text has to be $00-teminated, will be modified
; A/Y contains lo/hi of address
   jmp   convertascii

jt_printascii:
; $9FEB: prints out ascii text by converting [A-Z] <-> [a-z]
; text has to be $00-teminated, will not be modified
; A/Y contains lo/hi of address
   jmp   printascii

jt_print:
; $9FEE: prints out text followed by the jsr to this routine
; special characters:
; - $00: end of text
; - $01,$yy,$xx: prints $xx for $yy times
; - $02,$yy,$xx: sets the cursor to row $yy and column $xx
   jmp   print

jt_tftp_load_from_basic:
; $9FF1: this routine will load a file via tftp
; server ip is determined using dhcp "next-ip" (pxe boot server)
; filename is stored in $0380-$037e (127 bytes, null terminated if shorter)
; this allows for a dhcp/tftp driven boot menu
; a basic wrapper exists in the form of sys333,"filename"
; filename will be converted from petscii to ascii:
; case of letters will be toggled
   jmp   tftp_load_from_basic

.segment "CODE"

area_align_end_16:
   lda   AREA_END+0
   and   #$01
   beq   :+
   lda   #$00
   tay
   sta   (AREA_END),y
   inc   AREA_END+0
   bne   :+
   inc   AREA_END+1
:
   rts

area_check_end_16:
   lda   AREA_START+0
   cmp   AREA_END+0
   bne   :+
   lda   AREA_START+1
   cmp   AREA_END+1
:
   rts

area_set_start:
   lda   #<ETHER_FRAME_START
   sta   AREA_START+0
   lda   #>ETHER_FRAME_START
   sta   AREA_START+1
   rts

; AREA_END = AREA_START + X/Y (lo/hi)
area_set_end:
   clc
   txa
   adc   AREA_START+0
   sta   AREA_END+0
   tya
   adc   AREA_START+1
   sta   AREA_END+1
   rts

area_set_end_udp:
   clc
   lda   #<IP_PACKAGE_UDP_START
   adc   IP_PACKAGE_UDP_LENGTH+1
   sta   AREA_END+0
   lda   #>IP_PACKAGE_UDP_START
   adc   IP_PACKAGE_UDP_LENGTH+0
   sta   AREA_END+1
   rts

; x/a return lo/hi of package size
area_get_size:
   sec
   lda   AREA_END+0
   sbc   AREA_START+0
   tax
   lda   AREA_END+1
   sbc   AREA_START+1
   rts

.if USE_COMPACT_CODE
advance_area_start_by2:
   inc   AREA_START+0
   bne   advance_area_start
   inc   AREA_START+1
advance_area_start:
   inc   AREA_START+0
   bne   :+
   inc   AREA_START+1
:
   rts
.endif

advance_area_start_a:
   clc
   adc   AREA_START+0
   sta   AREA_START+0
   bcc   :+
   inc   AREA_START+1
:
   rts

advance_temp_a_1:
   inc   TEMP_A_16+0
   bne   :+
   inc   TEMP_A_16+1
:
   rts


clear_output:
; clear the bottom 360 bytes of the screen
   ldx   #$b4
   lda   #$20
:
   sta   $0680-1,x
   sta   $0734-1,x
   dex
   bne   :-
   rts

hexbyte_out:
   pha
   lsr
   lsr
   lsr
   lsr
   jsr   hexnib_out
   pla
   and   #$0f
hexnib_out:
   ora   #$30
   cmp   #$3a
   bcc   :+
   adc   #$06
   pha
   lda   $d018
   and   #$02
   lsr
   tax
   pla
   adc   @addtable,x
:
   jmp   BSOUT
@addtable:
   .byte $00,$20

print_dec:
   tax
   tya
   pha
   lda   #$00
   jsr   AXOUT
   pla
   tay
   rts

clear_netbuffer:
   ldx   #$00
:
   lda   #$00
   sta   BUFFER+$000,x
   sta   BUFFER+$100,x
   sta   BUFFER+$180,x
   lda   #$0b
   sta   $d800,x
   sta   $d900,x
   sta   $d980,x
   inx
   bne   :-
   rts

print:
   pla
   sta   TEMP_A_16+0
   pla
   sta   TEMP_A_16+1
@mainloop:
   ldy   #$00
   jsr   advance_temp_a_1
   lda   (TEMP_A_16),y
   bne   @evaluate
   lda   TEMP_A_16+1
   pha
   lda   TEMP_A_16+0
   pha
   rts

@evaluate:
   cmp   #$01           ; CTRL-A number char: print number of chars
   bne   @norle
   jsr   advance_temp_a_1
   lda   (TEMP_A_16),y
   tax
   jsr   advance_temp_a_1
   lda   (TEMP_A_16),y
@loopout:
   jsr   BSOUT
   dex
   bne   @loopout
   beq   @mainloop

@norle:
   cmp   #$02           ; CTRL-B line column: position cursor
   bne   @nopos
   jsr   advance_temp_a_1
   lda   (TEMP_A_16),y
   tax
   jsr   advance_temp_a_1
   lda   (TEMP_A_16),y
   tay
   clc
   jsr   PLOT
   jmp   @mainloop
@nopos:
   jsr   BSOUT
   jmp   @mainloop   ; always true

petscii_to_ascii:
   ; just converts uppercase<->lowercase letters, leaving the rest of
   ; the characters as they are, so
   ; e.g. the pound sign is the petscii equivalent for the backslash

   ; $41-$5a = a-z -> $61-$7a
   cmp   #$41
   bcc   @done
   cmp   #$5b
   bcs   :+
   ora   #$20
   bne   @done
:
   ; $61-$7a = A-Z -> $41-$5a
   cmp   #$61
   bcc   @done
   cmp   #$7b
   bcs   :+
   and   #$df
   bne   @done
:
   ; $c1-$da = A-Z -> $41-$5a
   cmp   #$c1
   bcc   @done
   cmp   #$db
   bcs   @done
   and   #$7f

@done:
   cmp   #$00
_asciiend:
   rts

printascii:
   sta   TEMP_A_16+0
   sty   TEMP_A_16+1
   ldy   #$00
:
   lda   (TEMP_A_16),y
   beq   _asciiend
   jsr   petscii_to_ascii
   jsr   BSOUT
   inc   TEMP_A_16+0
   bne   :-
   inc   TEMP_A_16+1
   bne   :-
;   rts   ; should never be reached

convertascii:
   sta   TEMP_A_16+0
   sty   TEMP_A_16+1
   ldy   #$00
:
   lda   (TEMP_A_16),y
   beq   _asciiend
   jsr   petscii_to_ascii
   sta   (TEMP_A_16),y
   inc   TEMP_A_16+0
   bne   :-
   inc   TEMP_A_16+1
   bne   :-
;   rts   ; should never be reached
