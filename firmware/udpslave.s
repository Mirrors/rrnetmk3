
; (c)opyright 2014-2017
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

.include "rrnetmk3.inc"

.segment "CODE"

IP_PACKAGE_SENDUDP_SERVICE       := IP_PACKAGE_UDP_PAYLOAD + $00
IP_PACKAGE_SENDUDP_SEQUENCE      := IP_PACKAGE_UDP_PAYLOAD + $01
IP_PACKAGE_SENDUDP_ADDRESS       := IP_PACKAGE_UDP_PAYLOAD + $02
IP_PACKAGE_SENDUDP_SIZE          := IP_PACKAGE_UDP_PAYLOAD + $04
IP_PACKAGE_SENDUDP_DATA          := IP_PACKAGE_UDP_PAYLOAD + $05

handle_udp_sendudp:
   ; check if mode is unset
   lda   TRANSFER_MODE
   bne   @check_mode_sendudp
   ; yes, set mode to sendudp
   jsr   save_remote_ip_and_mac
   lda   #TRANSFER_MODE_SENDUDP
   sta   TRANSFER_MODE
@check_mode_sendudp:
   ; check if mode is sendudp
   cmp   #TRANSFER_MODE_SENDUDP
   bne   @exit
   lda   IP_PACKAGE_SENDUDP_ADDRESS+0
   sta   TEMP_B_16+0
   lda   IP_PACKAGE_SENDUDP_ADDRESS+1
   sta   TEMP_B_16+1

   lda   IP_PACKAGE_UDP_SRC_PORT+0
   sta   PORT_REMOTE+0
   lda   IP_PACKAGE_UDP_SRC_PORT+1
   sta   PORT_REMOTE+1

   lda   IP_PACKAGE_SENDUDP_SEQUENCE
   sta   SENDUDP_SEQUENCE
   lda   IP_PACKAGE_SENDUDP_SERVICE
   sta   SENDUDP_SERVICE_ID

   cmp   #'L'
   beq   sendudp_load
.if 0
   cmp   #'S'
   beq   sendudp_save
.endif
   cmp   #'R'
   beq   sendudp_run
   cmp   #'J'
   beq   sendudp_jump

@exit:
   jmp   receive_loop

sendudp_load:
   sei
   lda   #$33
   sta   $01
   ldy   #$00
:
   lda   IP_PACKAGE_SENDUDP_DATA,y
   sta   (TEMP_B_16),y
   iny
   cpy   IP_PACKAGE_SENDUDP_SIZE
   bcc   :-
   lda   #$37
   sta   $01
   cli

   clc
   lda   TEMP_B_16+0
   adc   IP_PACKAGE_SENDUDP_SIZE
   sta   TEMP_B_16+0
   lda   TEMP_B_16+1
   adc   #$00
   sta   TEMP_B_16+1
   jsr   current_address_out

.if 1
   jsr   recall_cancel
.else
; do NOT use this: udpslave does not expect resending lost packages
@sendudp_resend_ack:
   lda   #<@sendudp_resend_ack
   ldy   #>@sendudp_resend_ack
   jsr   recall_after_2s
.endif

   lda   SENDUDP_SERVICE_ID
   sta   IP_PACKAGE_SENDUDP_SERVICE
   lda   SENDUDP_SEQUENCE
   sta   IP_PACKAGE_SENDUDP_SEQUENCE

   jsr   sendudp_ack
   jmp   receive_loop

sendudp_run:
   jsr   sendudp_ack
   jmp   disable_and_run
sendudp_jump:
   jsr   sendudp_ack
   jmp   disable_and_jump

sendudp_ack:
   lda   #<IP_PACKAGE_SENDUDP_SIZE
   sta   AREA_END+0
   lda   #>IP_PACKAGE_SENDUDP_SIZE
   sta   AREA_END+1

   lda   PORT_REMOTE+0
   sta   IP_PACKAGE_UDP_DEST_PORT+0
   lda   PORT_REMOTE+1
   sta   IP_PACKAGE_UDP_DEST_PORT+1
   lda   #>SENDUDP_PORT
   sta   IP_PACKAGE_UDP_SRC_PORT+0
   lda   #<SENDUDP_PORT
   sta   IP_PACKAGE_UDP_SRC_PORT+1

   jsr   transfer_ip_reply
   jsr   area_set_start
   jmp   udp_send_package
