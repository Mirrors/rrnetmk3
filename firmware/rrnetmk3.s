
; (c)opyright 2014-2018
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

.include "rrnetmk3.inc"

.segment "IDENT"
   .word   reset
   .word   nmi_handler
   .byte   $C3,$C2,$CD,$38,$30   ; petscii "CBM80"

write_tftp_load:
; intended to call with "SYS32777" after banking the ROM in
   jsr   copy_tftp_load
   memcpy7 exitcode_start,exitcode_end,EXEC_BUFFER
   jmp   EXEC_BUFFER

.segment "CODE"

copy_tftp_load:
   memcpy7 tftp_load_ram_start,tftp_load_ram_end,333
   rts

tftp_load_from_basic:
   jsr   calculate_filename_checksum
   sta   FILENAME_CHECKSUM
   jmp   reset

tftp_load_ram_start:
;  RAM CODE starts here
;  needs to be run with #$37 -> $01 and cartridge off
   jsr   CHKCOM
   jsr   FRMEVL
   jsr   FRESTR
   ; A now contains length of string
   ; $22/$23 vector to string
   cmp   #$80
   bcc   :+
   ldx   #STRING_TOO_LONG_ERROR
   jmp   BASERR
:
   tay
   dey
:
   lda   ($22),y
   sta   FILENAME_BUFFER,y
   dey
   bpl   :-
   lda   #$00
   sta   RRCTRL
   sta   RRN3_ROM_ENABLE
   jmp   jt_tftp_load_from_basic
tftp_load_ram_end:

.if USE_CRC16

; crc-16 calculation taken from
; http://www.6502.org/source/integers/crc-more.html

calc_crc16:
   ldy   #$ff
   sty   TEMP_A_16+0
   sty   TEMP_A_16+1
   iny
   lda   #$80
   sty   LOAD_VEC+0
   sta   LOAD_VEC+1
:
   lda   (LOAD_VEC),y
   eor   TEMP_A_16+1 ; A contained the data
   sta   TEMP_A_16+1 ; XOR it into high byte
   lsr               ; right shift A 4 bits
   lsr               ; to make top of x^12 term
   lsr               ; ($1...)
   lsr
   tax               ; save it
   asl               ; then make top of x^5 term
   eor   TEMP_A_16+0 ; and XOR that with low byte
   sta   TEMP_A_16+0 ; and save
   txa               ; restore partial term
   eor   TEMP_A_16+1 ; and update high byte
   sta   TEMP_A_16+1 ; and save
   asl               ; left shift three
   asl               ; the rest of the terms
   asl               ; have feedback from x^12
   tax               ; save bottom of x^12
   asl               ; left shift two more
   asl               ; watch the carry flag
   eor   TEMP_A_16+1 ; bottom of x^5 ($..2.)
   sta   TEMP_A_16+1 ; save high byte
   txa               ; fetch temp value
   rol               ; bottom of x^12, middle of x^5!
   eor   TEMP_A_16+0 ; finally update low byte
   ldx   TEMP_A_16+1 ; then swap high and low bytes
   sta   TEMP_A_16+1
   stx   TEMP_A_16+0
   iny
   bne   :-
   inc   LOAD_VEC+1
   lda   LOAD_VEC+1
   cmp   #$a0
   bne   :-
   rts
.endif


brk_handler:
   inc   $d020
   jmp   brk_handler

nmi_handler:
   rti

reset_vectors:
   ldy   #$1F
:
   lda   $FD30,y
   sta   $0314,y
   dey
   bpl   :-
   rts

.if USE_CHAMELEON_DETECTION
detect_chameleon:
   lda   TC64_CFGENA
   ldy   #$2a
   sty   TC64_CFGENA
   ldy   TC64_CFGENA
   iny
   sty   IS_CHAMELEON
   cmp   #$ff
   bne   :+
   sta   TC64_CFGENA
:
   rts
.endif

chameleon_turbo_on:
   lda   #$80
   .byte $2c
chameleon_turbo_off:
   lda   #$00
   ldx   #$2a
   stx   TC64_CFGENA
   sta   TC64_CFGTUR
   stx   TC64_CFGENA
   rts

gen_random16:
   lda   CIA1_BASE+CIA_TIMER_A+0
   sta   RANDOM16+0
   lda   CIA1_BASE+CIA_TIMER_A+1
   sta   RANDOM16+1
set_random16:
   lda   RANDOM16+0
   ora   RANDOM16+1
   beq   gen_random16
   rts

set_basic_vectors:
   jsr   reset_vectors
   jsr   SCRINI

.if USE_FAKE_STARTUP
   ; msg: COMMODORE BASIC V2
   lda   #$73
   ldy   #$E4
   jsr   TXTOUT
   ; 38911
   ldx   #$ff
   lda   #$97
   jsr   AXOUT
   ; BASIC BYTES FREE
   lda   #$60
   ldy   #$E4
   jsr   TXTOUT

   jsr   print_rrnet_message
   ; READY
   lda   #$76
   ldy   #$A3
   jsr   TXTOUT
.else
   jsr   print_rrnet_message
.endif

   lda   TEMP_B_16+0
   cmp   #<(EXEC_BUFFER+execute_run-disable_rom_code_start)
   bne   @sys
   lda   TEMP_B_16+1
   cmp   #>(EXEC_BUFFER+execute_run-disable_rom_code_start)
   bne   @sys
   jsr   print
   .byte "RUN",$00
   bne   @done
@sys:
   jsr   print
   .byte "SYS",$00
   ldx   TEMP_B_16+0
   lda   TEMP_B_16+1
   jsr   AXOUT
@done:
   lda   #$0D
   jsr   BSOUT
   lda   LOAD_VEC+0
   ldx   LOAD_VEC+1
   sta   BASIC_END+0
   stx   BASIC_END+1
   sta   VARIABLE_END+0
   stx   VARIABLE_END+1
   sta   ARRAY_END+0
   stx   ARRAY_END+1
clean_vectors:
   jsr   IOINI
   jsr   CIAINI
   jmp   BASINI

.if 0
advance_area_end_8:
   inc   AREA_END+0
   bne   :+
   inc   AREA_END+1
:
   rts
.endif

print_rrnet_message:
   jsr   print
   .byte $0D,$01,$03,$1D,"RR-NET USING MAC ",$00
   jsr   print_mac_address
   jsr   print
   .byte $0D,$1D,$1D,$1D,"HOLD ",$12," CTRL ",$92," AT RESET FOR HELP/MENU"
   .byte $0D,$00
   rts

; ==================================================
; = reset routine, handle startup, keys held, etc. =
; ==================================================
reset:
   sei
   ldx   #$FF
   txs
   cld
   jsr   IOINI
   jsr   chameleon_turbo_on
   jsr   disable_freeze         ; disable RR freeze button

   lda   #$00
   tay
:
   sta   $0002,y
   sta   $0200,y
   sta   $0280,y
   iny
   bne   :-
   jsr   calculate_filename_checksum
   cmp   FILENAME_CHECKSUM
   beq   @noclearfilename
   tya   ; lda #$00
:
   sta   $0300,y
   iny
   bne   :-
@noclearfilename:
   jsr   copy_tftp_load
   ldx   #$3C
   ldy   #$03
   stx   $B2
   sty   $B3
   ldx   #$00
   ldy   #$A0
   jsr   MEMSET
   jsr   CIAINI
   jsr   reset_vectors
; copy the last 12 bytes (IP+MAC+CHKSUM) to ram for third party software
   ldx   #$0b
:
   lda   ROM_IP_ADDRESS,x
   sta   RAM_IP_ADDRESS,x
   dex
   bpl   :-
   lda   #<brk_handler
   sta   BRKVEC+0
   lda   #>brk_handler
   sta   BRKVEC+1
   lda   #<nmi_handler
   sta   NMIVEC+0
   lda   #>nmi_handler
   sta   NMIVEC+1
   jsr   SCRINI
.if 1
   jsr   reset_screen
.else
   lda   #$06
   sta   $d020
   lda   #$00
   sta   $d021
.endif
;   cli
   jsr   VECINI
   jsr   BASINI
; to make sure vectors match upon exit
   lda   #$03
   sta   LOAD_VEC+0
   lda   #$08
   sta   LOAD_VEC+1

   ldx   #$FB
   txs
   jsr   enable_clockport       ; enable RR clockport connector
   jsr   cs8900a_check_hw
   bpl   @checksum_okay
   jmp   hardware_not_found

@checksum_okay:
   jsr   cs8900a_init

@check_for_shift_key:
   ldx   #$FF
   stx   CIA1_BASE+CIA_DDRA
   inx
   stx   CIA1_BASE+CIA_DDRB

   lda   #$FD
   sta   CIA1_BASE+CIA_PRA
   lda   CIA1_BASE+CIA_PRB
   cmp   #$7F
   bne   @check_for_cbm_key
@jmp_run_server_dhcp:
   jmp   run_server_dhcp

@check_for_cbm_key:
   lda   #DHCP_STATE_OFF
   sta   DHCP_STATE
   lda   #$7F
   sta   CIA1_BASE+CIA_PRA
   lda   CIA1_BASE+CIA_PRB
   cmp   #$DF           ; C= keycode
   bne   @check_for_ctrl_key
@jmp_run_server_enter_ip:
   jsr   enter_ip_address
@jmp_run_server_default_ip:
   jmp   run_server_static_ip

@check_for_ctrl_key:
   cmp   #$FB           ; CTRL keycode
   bne   @boot_default
@jmp_goto_menu:
   jmp   goto_menu

@boot_default:
   ; check if PXE-with-replace-filename is active
   jsr   calculate_filename_checksum
   cmp   FILENAME_CHECKSUM
   beq   @jmp_run_server_dhcp

   ldx   bootdefault
   beq   disable_and_basic          ; 0
   dex
   beq   @jmp_goto_menu             ; 1
   dex
   beq   @jmp_run_server_dhcp       ; 2
   dex
   beq   @jmp_run_server_enter_ip   ; 3
   dex
   beq   @jmp_run_server_default_ip  ; 4
; slip through: run floppy autoboot ; 5
; TODO: wait for IEC-bus to settle

   lda   #$02
   .byte $2c      ; skip next instruction

disable_and_basic:
   lda   #$00
   sta   $c6
   ;jsr   clean_vectors
   jsr   reset_vectors
   jsr   SCRINI
   jsr   CIAINI
   jsr   VECINI
   jsr   BASINI
   jsr   RSMOUT

   jsr   print_rrnet_message
   memcpy7 exitcode_start,exitcode_end,EXEC_BUFFER
   
   lda   $c6
   cmp   #$02                    ; one keystroke may by from other mode
   bcc   @noautoload8

   jsr   print
   .byte $0d,$0d,"LOAD",$22,":*",$22,",8,1"
   .byte $01,$05,$0d,"RUN",$01,$07,$91,$00

   jsr   chameleon_turbo_off
   lda   #$08
   sta   TEMP_A_16
   ldx   #$00
   ldy   #$00
:
   inx
   bne   :-
   iny
   bne   :-
   dec   TEMP_A_16
   bne   :-

   lda   #$0d
   sta   $0277
   sta   $0278

@noautoload8:
   jmp   EXEC_BUFFER

   ; this must run in RAM
exitcode_start:
   lda   #$02
   sta   RRCTRL                 ; disable RR, use bank 0
   sta   RRN3_ROM_DISABLE       ; disable RR-Net MK3 ROM
   ldx   #$FB
   txs
   jmp   WSTART
exitcode_end:

disable_and_run:
   lda   #<(EXEC_BUFFER+execute_run-disable_rom_code_start)
   sta   TEMP_B_16+0
   lda   #>(EXEC_BUFFER+execute_run-disable_rom_code_start)
   sta   TEMP_B_16+1
disable_and_jump:
   jsr   set_basic_vectors
   memcpy7 disable_rom_code_start,disable_rom_code_end,EXEC_BUFFER
   lda   #$FF
   sta   $3A
   jsr   chameleon_turbo_off
   lda   $A003
   pha
   ldx   $A002
   dex
   txa
   pha
   lda   #$00
   tax
   tay
   jmp   EXEC_BUFFER

   ; this must run in RAM
disable_rom_code_start:
   lda   #$02
   sta   RRCTRL                 ; disable RR, use bank 0
   sta   RRN3_ROM_DISABLE       ; disable RR-Net MK3 ROM
   jmp   (TEMP_B_16)
execute_run:
   jsr   BASCLR
   jsr   RELINK
   jmp   NXTSTM
disable_rom_code_end:

set_ip_header_checksum:
   lda   #$00
   sta   IP_HEADER_HEADER_CKSUM+0
   sta   IP_HEADER_HEADER_CKSUM+1
   jsr   calculate_ip_header_checksum
   lda   TEMP_A_16+0
   eor   #$ff
   sta   IP_HEADER_HEADER_CKSUM+0
   lda   TEMP_A_16+1
   eor   #$ff
   sta   IP_HEADER_HEADER_CKSUM+1
   rts

calculate_udp_checksum:
; seed with sum of length and protocol
   lda   #<IP_HEADER_SRC_IP_ADDR
   sta   AREA_START+0
   lda   #>IP_HEADER_SRC_IP_ADDR
   sta   AREA_START+1

   clc
   ldy   IP_PACKAGE_UDP_LENGTH+0
   lda   IP_PACKAGE_UDP_LENGTH+1
   adc   #$11
   bcc   :+
   iny
   clc
:

calculate_checksum:
   sty   TEMP_A_16+0
   sta   TEMP_A_16+1
   php               ; saving the carry to the stack for loop to collect
@loop:
   ldy   #$00
   plp
   lda   TEMP_A_16+0
   adc   (AREA_START),y
   sta   TEMP_A_16+0
   iny
   lda   TEMP_A_16+1
   adc   (AREA_START),y
   sta   TEMP_A_16+1
   php
.if USE_COMPACT_CODE
   jsr   advance_area_start_by2
   jsr   area_check_end_16
   bne   @loop
.else
   ; this should not trigger a page wrap
   inc   AREA_START+0
   inc   AREA_START+0 ; only second needs to be cheched because we're 16bit aligned
   bne   :+
   inc   AREA_START+1
:
   lda   AREA_START+1
   cmp   AREA_END+1
   bne   @loop
   lda   AREA_START+0
   cmp   AREA_END+0
   bne   @loop
.endif

   plp
; add final carry
   bcc   :+
   inc   TEMP_A_16+0
   bne   :+
   inc   TEMP_A_16+1
:
   rts

.if ENABLE_IGMP
calculate_igmp_checksum:
   clc
   lda   #<IP_PACKAGE_IGMP_START
   sta   AREA_START+0
   adc   #$20
   sta   AREA_END+0
   lda   #>IP_PACKAGE_IGMP_START
   sta   AREA_START+1
   adc   #$00
   sta   AREA_END+1
   lda   #$00
   tay
   beq   calculate_checksum
.endif

calculate_ip_header_checksum:
   lda   #$00
   sta   TEMP_A_16+0
   sta   TEMP_A_16+1
   ldy   #$13
   clc
:
   lda   TEMP_A_16+1
   adc   IP_HEADER_HEADER_LEN,y
   sta   TEMP_A_16+1
   dey
   lda   TEMP_A_16+0
   adc   IP_HEADER_HEADER_LEN,y
   sta   TEMP_A_16+0
   dey
   bpl   :-

; add final carry
   bcc   :+
   inc   TEMP_A_16+1
   bne   :+
   inc   TEMP_A_16+0
:
   rts

.if ENABLE_IGMP
handle_igmp_package:
   pha
   ldx   #$1C
:
   lda   IP_HEADER_HEADER_LEN+$00,x
   sta   IP_PACKAGE_IGMP_START+$04,x
   dex
   bpl   :-
   ldx   #$07
   lda   #$00
:
   sta   IP_PACKAGE_IGMP_START+$00,x
   dex
   bne   :-

   sta   IP_HEADER_TOTAL_LEN+$00
   lda   #$34
   sta   IP_HEADER_TOTAL_LEN+$01
   lda   #$01
   sta   IP_HEADER_PROTOCOL
   lda   #$03
   sta   IP_PACKAGE_IGMP_START+$00
   pla
   sta   IP_PACKAGE_IGMP_START+$01

   jsr   calculate_igmp_checksum

   lda   TEMP_A_16+0
   eor   #$FF
   sta   IP_PACKAGE_IGMP_START+$02
   lda   TEMP_A_16+1
   eor   #$FF
   sta   IP_PACKAGE_IGMP_START+$03
   jsr   package_ip_reply
   jmp   codenet_send_ack
.endif

enter_ip_address:
   jsr   print
   .byte $93
   .byte $02,$14,$01,"eNTER ip ADDRESS",$00
   jsr   print_hw_info
   lda   #$9f           ; color cyan
   jsr   BSOUT
   lda   #$00
   sta   $D4
   cli
   jsr   BASIN
   lda   #$00
   sta   $D4
   jsr   print
   .byte $9e,$0d,$00
   jmp   read_ip_from_screen

print_mac_address:
   ldy   #$00
:
   lda   ROM_MAC_ADDR,y
   sta   RAM_MAC_ADDR,y
   jsr   hexbyte_out
   iny
   cpy   #$06
   bcs   @exit
   lda   #':'
   jsr   BSOUT
   bne   :-
@exit:
   rts

print_ip_address:
   ldy   #$00
:
   lda   RAM_IP_ADDRESS,y
   jsr   print_dec
   iny
   cpy   #$04
   bcs   @exit
   lda   #'.'
   jsr   BSOUT
   bne   :-
@exit:
   rts

input_find_non_number:
   ldy   #$00
:
   lda   ($22),y
   cmp   #$30
   bcc   :+
   cmp   #$3A
   bcs   :+
   iny
   bne   :-
:
   tya
   rts

input_string_to_ax:
; convert input to 16-bit value
   jsr   VALFNC
   jsr   FL2INT
   ldx   $64
   lda   $65
   rts

input_skip_spaces:
   lda   #$20
   ldy   #$00
:
   cmp   ($22),y
   bne   :++
   inc   $22
   bne   :+
   inc   $23
:
   bne   :--
:
   lda   $22
   ldx   $23
   rts

move_input_vector:
   tax
   inx
   txa
   clc
   adc   $22
   sta   $22
   bcc   :+
   inc   $23
:
   rts

read_ip_from_screen:
; read ip-address from screen memory (!)
   lda   #<IP_ADDRESS_ON_SCREEN
   ldx   #>IP_ADDRESS_ON_SCREEN
   sta   $22
   stx   $23
   jsr   input_skip_spaces
   jsr   input_find_non_number
   pha
   jsr   input_string_to_ax
   sta   RAM_IP_ADDRESS+$00
   pla
   jsr   move_input_vector
   jsr   input_skip_spaces
   jsr   input_find_non_number
   pha
   jsr   input_string_to_ax
   sta   RAM_IP_ADDRESS+$01
   pla
   jsr   move_input_vector
   jsr   input_skip_spaces
   jsr   input_find_non_number
   pha
   jsr   input_string_to_ax
   sta   RAM_IP_ADDRESS+$02
   pla
   jsr   move_input_vector
   jsr   input_skip_spaces
   jsr   input_find_non_number
   jsr   input_string_to_ax
   sta   RAM_IP_ADDRESS+$03
   rts

enable_clockport:
   lda   RREXTD
   ora   #$01
   sta   RREXTD
   rts

disable_freeze:
   lda   RREXTD
   ora   #$04
   sta   RREXTD
   rts

;.if 0
;disable_ram:
;   lda   RREXTD
;   and   #$FE
;   sta   RREXTD
;   rts
;.endif

current_address_out:
   jsr   print
   .byte $02,$16,$20,"WRITING"
   .byte $02,$17,$20,"TO:",$00
   lda   TEMP_B_16+1
   jsr   hexbyte_out
   lda   TEMP_B_16+0
   jmp   hexbyte_out

set_udp_checksum:
   lda   #$00
   sta   IP_PACKAGE_UDP_CHECKSUM+0
   sta   IP_PACKAGE_UDP_CHECKSUM+1
   jsr   calculate_udp_checksum
   lda   TEMP_A_16+0
   eor   #$ff
   sta   IP_PACKAGE_UDP_CHECKSUM+0
   lda   TEMP_A_16+1
   eor   #$ff
   sta   IP_PACKAGE_UDP_CHECKSUM+1
   rts

goto_menu:
; create the sprite for the o umlaut
   lda   #$00
   ldx   #$3f
:
   sta   $03c0,x
   dex
   bne   :-
   lda   #$66
   sta   $03c0

   jsr   print
   .byte $08,$0e,$93
   .byte $02,$01,$01,$05,$01,$04,"* rETRO rEPLAY nET (R"
   .byte SVNREVISION
   .byte ") ",$01,$04,"*",$9a
   .byte $02,$03,$01,"pRESS ",$12," s ",$92," TO MANUALLY SET ipV4 ADDRESS"
   .byte $02,$04,$01,"(OR HOLD ",$12," c= ",$92," DURING RESET)"
   .byte $02,$06,$01,"pRESS ",$12," d ",$92," TO USE dhcpV4 TO CONFIGURE"
   .byte $02,$07,$01,"(OR HOLD LEFT ",$12," shift ",$92," DURING RESET)"
   .byte $02,$09,$01,"pRESS ",$12," run/stop ",$92," TO EXIT"
   .byte $02,$0b,$00,$1f,$12,$01,$50,$20,$92,$9a
   .byte $02,$0e,$01,"pROTOCOLS SUPPORTED (USING udp PORT):"
   .byte     $0d,$1d,"cODENET(6264),udpsLAVE(3172),tftp(69)"
   .byte $02,$11,$01,"hARDWARE BY jENS sCHONFELD"
   .byte $02,$12,$01,"fIRMWARE BY sVoLLI WITH HELP AND CODE"
   .byte $02,$13,$02,"FROM tOBIAS kORBMACHER AND jOHN sELCK"
   .byte $02,$15,$03,"(C)2014-2018 iNDIVIDUAL cOMPUTERS"
   .byte $02,$16,$0a,"aLL RIGHTS RESERVED"
   .byte $02,$17,$01,$05,"vISIT HTTP://WIKI.ICOMP.DE/WIKI/rr-nET",$9a
.if USE_CRC16
   .byte $02,$0a,$1d,"crc16:"
.endif
   .byte $00

; set o-umlaut sprite position and color
   lda   $dabd
   sta   $d027
   lda   #$0f
   sta   $07f8
   lda   #$c0
   sta   $d000
   lda   #$ba
   sta   $d001
   lda   #$01
   sta   $d015

.if USE_CRC16
   cli
   jsr   calc_crc16
   lda   TEMP_A_16+1
   jsr   hexbyte_out
   lda   TEMP_A_16+0
   jsr   hexbyte_out
.endif

   cli
@keyloop:
   jsr   GETIN
   cmp   #'S'
   beq   start_set_ip
   cmp   #'D'
   beq   start_dhcp
   cmp   #'O'
   bne   :+
   jsr   chameleon_turbo_off
:
   cmp   #$03
   bne   @keyloop

   jsr   reset_screen
   jmp   disable_and_basic

reset_screen:
   jsr   print
   .byte $93,$8e,$09,$00 ; reset charset
   lda   #$06
   sta   $d020
   lda   #$00
   sta   $d021
;   lda   #$00
   sta   $d015
   rts

start_set_ip:
   lda   #DHCP_STATE_OFF
   sta   DHCP_STATE
   jsr   reset_screen
   jmp   run_server_enter_ip

start_dhcp:
   lda   #DHCP_STATE_DISCOVER
   sta   DHCP_STATE
   jsr   reset_screen
   jmp   run_server_dhcp

print_hw_info:
   jsr   print
   .byte $08,$0e ; ctrl-h, ctrl-n: make charset lowercase
   .byte $02,$16,$01,"mac aDDRESS: ",$00
   jsr   print_mac_address

   jsr   print
   .byte $02,$17,$01,"ip  aDDRESS:",$9F,$01,$11," ",$01,$10,$9D,$00
   jsr   print_ip_address
   lda   #$9A
   jmp   BSOUT

print_status:
; upon return sets negative flag with link status:
; 1 = on, 0 = off, bmi link_active
.if USE_DEBUG
; RxEvent:                                                   Y | X
; - ExtraData Runt CRCerror|Broadcast IndividualAdr Hashed RxOK|DribbleBits IAHash
; 8     4      2      1    |    8          4          2     1  |     8        4
   cs8900a_read_page CS_PP_RXEVENT
   sty   TEMP_A_16+0
   stx   TEMP_A_16+1

   lda   TEMP_A_16+0
   ldx   #$06
   ldy   #$05
   jsr   rev_on_bit

   lda   TEMP_A_16+0
   ldx   #$05
   ldy   #$06
   jsr   rev_on_bit

   lda   TEMP_A_16+0
   ldx   #$04
   ldy   #$07
   jsr   rev_on_bit

   lda   TEMP_A_16+0
   ldx   #$03
   ldy   #$08
   jsr   rev_on_bit

   lda   TEMP_A_16+0
   ldx   #$02
   ldy   #$09
   jsr   rev_on_bit

   lda   TEMP_A_16+0
   ldx   #$00
   ldy   #$0a
   jsr   rev_on_bit

   lda   TEMP_A_16+1
   ldx   #$07
   ldy   #$0b
   jsr   rev_on_bit

; BusST:               Y | X
; - - - -|- - - Rdy4TxNOW|TxBidErr -
; 8 4 2 1|8 4 2     1    |   8     4
   cs8900a_read_page CS_PP_BUSST
   sty   TEMP_A_16+0
   stx   TEMP_A_16+1

   lda   TEMP_A_16+0
   ldx   #$00
   ldy   #$11
   jsr   rev_on_bit

   lda   TEMP_A_16+1
   ldx   #$07
   ldy   #$12
   jsr   rev_on_bit
.endif

; LineST:                      Y | X
; - CRS - PolarityOK|- - 10BT AUI|LinkOK -
; 8  4  2     1     |8 4  2    1 |  8    4
   cs8900a_read_page CS_PP_LINEST
.if USE_DEBUG
   sty   TEMP_A_16+0
.endif
   stx   TEMP_A_16+1

.if USE_DEBUG
   lda   TEMP_A_16+0
   ldx   #$06
   ldy   #$18
   jsr   rev_on_bit

   lda   TEMP_A_16+0
   ldx   #$04
   ldy   #$19
   jsr   rev_on_bit

   lda   TEMP_A_16+0
   ldx   #$01
   ldy   #$1a
   jsr   rev_on_bit
.endif

   ldx   #$03
   lda   #$02           ; red
   bit   TEMP_A_16+1
   bpl   :+
   lda   #$05           ; green
:
   sta   $daf9,x
   dex
   bpl   :-
   bit   TEMP_A_16+1
   rts

.if USE_DEBUG
rev_on_bit:
   lsr
   dex
   bpl   rev_on_bit
   lda   $0748,y
   and   #$7f
   bcc   :+
   ora   #$80
:
   sta   $0748,y
   bcc   :+
   lda   #$06
   sta   $db48,y
:
   rts
.endif

hardware_not_found:
   jsr   print      ;1234567890123456789012345678901234567890
   .byte $0e
   .byte $02,$01,$01,"eRROR:"
   .byte $02,$03,$01,"cRYSTAL cs8900a NETWORK CHIP NOT FOUND"
   .byte $02,$05,$01,"ON CLOCKPORT ($de02-$de0f)"
   .byte $02,$07,$01,"vISIT HTTP://WIKI.ICOMP.DE/WIKI/rr-nET"
   .byte $00
   cli
:
   jsr   GETIN
   beq   :-

   lda   RESET_VECTOR+0
   sta   TEMP_B_16+0
   lda   RESET_VECTOR+1
   sta   TEMP_B_16+1
   jmp   disable_and_jump

save_remote_ip_and_mac:
   ldx   #$05
:
   lda   IP_HEADER_SRC_IP_ADDR,x
   sta   IP_ADDRESS_REMOTE,x
   lda   ETHER_FRAME_SRC_MAC,x
   sta   MAC_ADDRESS_REMOTE,x
   dex
   bpl   :-
   rts

transfer_ip_reply:
   sec
   .byte $24
; switch ip address
package_ip_reply:
   clc
   ldx   #$03
@loop:
   bcs   @skipswap
   lda   IP_HEADER_SRC_IP_ADDR,x
   bcc   @skipcopy
@skipswap:
   lda   IP_ADDRESS_REMOTE,x
@skipcopy:
   sta   IP_HEADER_DEST_IP_ADDR,x
   lda   RAM_IP_ADDRESS,x
   sta   IP_HEADER_SRC_IP_ADDR,x
   dex
   bpl   @loop
   lda   #$80
   sta   IP_HEADER_TTL
   php
   jsr   set_ip_header_checksum
   plp

   .byte $24
package_ether_reply:
   clc
; switch mac address
   ldx   #$05
@loop:
   bcs   @skipswap
   lda   ETHER_FRAME_SRC_MAC,x
   bcc   @skipcopy
@skipswap:
   lda   MAC_ADDRESS_REMOTE,x
@skipcopy:
   sta   ETHER_FRAME_DEST_MAC,x
   lda   ROM_MAC_ADDR,x
   sta   ETHER_FRAME_SRC_MAC,x
   dex
   bpl   @loop
   rts

calculate_filename_checksum:
   ldx   #$7e
   lda   #$55
   clc
:
   adc   FILENAME_BUFFER,x
   dex
   bpl   :-
   rts

check_keys_pressed:
   ldx   #$FF
   stx   CIA1_BASE+CIA_DDRA
   inx
   stx   CIA1_BASE+CIA_DDRB
   lda   #$7F
   sta   CIA1_BASE+CIA_PRA
   cmp   CIA1_BASE+CIA_PRB ; check for run/stop
.if ALLOW_CHARSET_SWITCH
   beq   @done          ; runstop pressed

   lda   CIA1_BASE+CIA_PRB
   cmp   LAST_KEYCODE
   beq   @nokeychange
   sta   LAST_KEYCODE
   cmp   #$DF
   bne   @nocbm
   lda   $d018
   eor   #$02
   sta   $d018
@nocbm:
@nokeychange:
   lda   #$ff
@done:
.endif
   rts


.segment "FF4C"
; will be executed when pressing the freeze button on chameleon
; address $FF4C is used because the mac address if mirrored at the vectors
   sei
   ldx   #(disable_rom_code_end-disable_rom_code_start-1)
:
   lda   disable_rom_code_start | $E000,x
   sta   EXEC_BUFFER,x
   dex
   bpl   :-

; there is no simple way to detect the kernel reset vector, since were are
; in ultimax mode, so assume the known $FCE2
   lda   #$E2
   sta   TEMP_B_16+0
   lda   #$FC
   sta   TEMP_B_16+1
   jmp   EXEC_BUFFER

.segment "BOOTDEF"
bootdefault:
   .byte $00

.segment "ROCONFIG"

ROM_IP_ADDRESS:
   .byte 192,168,0,64

.if 0
; SvOlli's MAC
ROM_MAC_ADDR:
   .byte $28,$cd,$4c,$ff,$f8,$f3
ROM_CHKSUM:
   .byte $5e,$e3
.else
; generic MAC
ROM_MAC_ADDR:
   .byte $28,$cd,$4c,$ff,$ff,$ff
ROM_CHKSUM:
   .byte $a2,$33
.endif
