
; (c)opyright 2014-2017
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

.include "rrnetmk3.inc"

ARP_PACKAGE_START                := ETHER_FRAME_START + $0E
ARP_PACKAGE_HTYPE                := ARP_PACKAGE_START + $00
ARP_PACKAGE_PTYPE                := ARP_PACKAGE_START + $02
ARP_PACKAGE_HLEN                 := ARP_PACKAGE_START + $04
ARP_PACKAGE_PLEN                 := ARP_PACKAGE_START + $05
ARP_PACKAGE_OPER                 := ARP_PACKAGE_START + $06
ARP_PACKAGE_SHA                  := ARP_PACKAGE_START + $08
ARP_PACKAGE_SPA                  := ARP_PACKAGE_START + $0E
ARP_PACKAGE_THA                  := ARP_PACKAGE_START + $12
ARP_PACKAGE_TPA                  := ARP_PACKAGE_START + $18
ARP_PACKAGE_END                  := ETHER_FRAME_START + $1C

ARP_OPER_REQUEST                 := 1
ARP_OPER_REPLY                   := 2

reply_arp:
   ldx   #$07
:
   lda   @arp_reply_header,x
   cmp   ARP_PACKAGE_HTYPE+$00,x
   bne   @exit
   dex
   bpl   :-
   ldx   #$03
:
   lda   RAM_IP_ADDRESS,x
   cmp   ARP_PACKAGE_TPA,x
   bne   @exit
   dex
   bpl   :-
   lda   #ARP_OPER_REPLY
   sta   ARP_PACKAGE_OPER+$01
; move SHA+SPA to THA+TPA and
; copy SHA from mac address
   ldx   #$09
:
   lda   ARP_PACKAGE_SHA,x
   sta   ARP_PACKAGE_THA,x
   lda   ROM_MAC_ADDR,x
   sta   ARP_PACKAGE_SHA,x
   dex
   bpl   :-
; copy SPA from ip address
   ldx   #$03
:
   lda   RAM_IP_ADDRESS,x
   sta   ARP_PACKAGE_SPA,x
   dex
   bpl   :-
   jsr   package_ether_reply
   ldx   #$3C            ; TODO: shouldn't this be only $2a? ($0e ether+$1c arp)
   ldy   #$00            ; TODO: nope, but adding 00s from $2a-$3b would be nice
   jmp   cs8900a_send_ip_frame_16

@exit:
   rts

.segment "GAPDATA"

@arp_reply_header:
   .byte $00,$01,$08,$00,$06,$04,$00,$01 ; HTYPE(2),PTYPE(2),HLEN,PLEN,OPER(2)
