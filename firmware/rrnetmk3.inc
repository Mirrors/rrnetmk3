
; (c)opyright 2014-2017
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

; calculate and display crc16 checksum of rom
; disabled, because tool verifies every byte it writes
.define USE_CRC16                0

; use macros instead of lda/jsr
.define USE_MACROS               1

; check if the firmware is running on a chameleon and write state to ram
; disabled, because detection is not used anywhere
.define USE_CHAMELEON_DETECTION  0

; show a fake reset message after file has been transferred
; disabled to save ROM and more compatibility to 3rd party kernals
.define USE_FAKE_STARTUP         0

; include some debug code like flashing borders
.define USE_DEBUG                0

; use subroutines instead of unrolled code
.define USE_COMPACT_CODE         0

; USE_SPEED_CODE replaces code for transferring data with code
; optimized for speed instead of size
; write disabled, because it slows down instead of speeding up
.define USE_SPEED_CODE_WRITE     0
.define USE_SPEED_CODE_READ      1

; allow C= + Shift to toggle charsets
; disabled because font is already lowercase in server mode
.define ALLOW_CHARSET_SWITCH     0

; enable support for the IGMP protocol
; disabled because I could not find a usecase
.define ENABLE_IGMP              0

.include "../svnversion.inc"

; TODO:
; - disable NMI by keeping NMI line low
; - CODENET_SERVICE_PING ?
; - check why no reply to arping

; original code written by Tobias Korbmacher
; using codenet server by John Selck
; run through da65, additional documentation and code improvements by SvOlli
; improvements are:
; - Turbo Chameleon 64 compatible using Retro Replay bankswitching
; - Retro/Nordic Replay also tested
; - maximum ip package size increased from 199 to 640 bytes
; - DHCP client
; - all known protocols to upload code available
;   - codenet
;   - udpslave (originally by Per Olofsson)
;   - TFTP (RFC 1530)
;     - sys333,"filename" restarts firmware in dhcp-mode, but replaces
;       dhcp supplied filename with the parameter
; - combination of DHCP and TFTP possible for autoloading via network (PXE)
;   (filename must end in ".prg")

.macro cs8900a_write_page page, value
   lda #(page)/2
.if (.match (.left (1, value), #))   ; immediate mode
   ldx #<(.right (.tcount (value)-1, value))
   ldy #>(.right (.tcount (value)-1, value))
.else               ; assume absolute or zero page
   ldx value
   ldy 1+(value)
.endif
   jsr cs8900a_set_packet_page
.endmacro

.macro cs8900a_read_page page
   lda #(page)/2
   jsr cs8900a_get_packet_page
.endmacro

.setcpu "6502"

.define CODENET_SERVICE_PING  $00
.define CODENET_SERVICE_ACK   $01
.define CODENET_SERVICE_DATA  $04
.define CODENET_SERVICE_FILL  $05
.define CODENET_SERVICE_JUMP  $06
.define CODENET_SERVICE_RUN   $07
.define CODENET_MAGIC         $CA1F
.define CODENET_PORT          6462
.define SENDUDP_PORT          3172
.define BOOTPS_PORT           67
.define BOOTPC_PORT           68
.define TFTP_SERVER_PORT      69
.define TFTP_TRANSFER_PORT    234
.define ECHO_PORT             7

.define IP_ADDRESS_ON_SCREEN  $07A6

EXEC_BUFFER             := $010A
IRQVEC                  := $0314
BRKVEC                  := $0316
NMIVEC                  := $0318
BUFFER                  := $0400

RELINK                  := $A533
BASCLR                  := $A659
BASERR                  := $A43A
NXTSTM                  := $A7AE
CHKCOM                  := $AEFD
FRMEVL                  := $AD9E
TXTOUT                  := $AB1E
FRESTR                  := $B6A3
VALFNC                  := $B7B5
FL2INT                  := $BC9B
AXOUT                   := $BDCD
WSTART                  := $E386
BASINI                  := $E3BF
RSMOUT                  := $E422
VECINI                  := $E453
MEMSET                  := $FD8C
CIAINI                  := $FDA3
SCRINI                  := $FF81
IOINI                   := $FF84
MEMINI                  := $FF87
IOVINI                  := $FF8A
BASIN                   := $FFCF
BSOUT                   := $FFD2
GETIN                   := $FFE4
PLOT                    := $FFF0
RESET_VECTOR            := $FFFC

VIC_BASE                := $D000
VIC_CTRLX               := VIC_BASE+$16
VIC_COLBRD              := VIC_BASE+$20

TC64_CFGTUR             := $D0F3
TC64_CFGENA             := $D0FE

CIA1_BASE               := $DC00
CIA2_BASE               := $DD00
CIA_PRA                 := $00
CIA_PRB                 := $01
CIA_DDRA                := $02
CIA_DDRB                := $03
CIA_TIMER_A             := $04
CIA_TIMER_B             := $06
CIA_TOD_10TH            := $08
CIA_TOD_SEC             := $09
CIA_TOD_MIN             := $0A
CIA_TOD_HR              := $0B
CIA_ICR                 := $0D
CIA_CRA                 := $0E
CIA_CRB                 := $0F

RRN3_ROM_ENABLE         := $DE80
RRN3_ROM_DISABLE        := $DE88

RRCTRL                  := $DE00
RREXTD                  := $DE01

RXTX_DATA0              := $DE08
RXTX_DATA1              := $DE0A
TXCMD                   := $DE0C
TXLENGTH                := $DE0E
PP_POINTER              := $DE02
PP_DATA0                := $DE04
PP_DATA1                := $DE06

CS_PP_PRODINF           := $0000
CS_PP_RXCTL             := $0104
CS_PP_LINECTL           := $0112
CS_PP_SELFCTL           := $0114
CS_PP_RXEVENT           := $0124
CS_PP_LINEST            := $0134
CS_PP_BUSST             := $0138

FILE_NOT_FOUND_ERROR    := $04
STRING_TOO_LONG_ERROR   := $17
LOAD_ERROR              := $1d

.include "ether_ip.inc"

; temporary vectors / storage
AREA_START              := $03 ; $04
AREA_END                := $05 ; $06

LOAD_VEC                := $AE ; $AF
BASIC_END               := $2D ; $2E
VARIABLE_END            := $2F ; $30
ARRAY_END               := $31 ; $32

TI                      := $A0 ;-$A2
TEMP_A_16               := $FB ; $FC
TEMP_B_16               := $FD ; $FE
TEMP_8                  := $02

; these are fixed addresses to be used after start of payload
RAM_IP_ADDRESS          := $0334 ;-$0337
RAM_MAC_ADDR            := $0338 ;-$033D
RAM_MAC_CHKSUM          := $033E ;-$033F

; these addresses might change
; (these must follow each other, first ip, then mac)
IP_ADDRESS_REMOTE       := $0340 ;-$0343
MAC_ADDRESS_REMOTE      := $0344 ;-$0349
PORT_REMOTE             := $034A ; $034B

DHCP_STATE              := $034C

TFTP_EXPECTED_BLOCKNUM  := $034D ; blocknum can never be 16 bit in this usecase
CODENET_SERVICE_ID      := $034D ; can be same as TFTP_EXPECTED_BLOCKNUM
SENDUDP_SERVICE_ID      := $034D ; can be same as TFTP_EXPECTED_BLOCKNUM

TFTP_LOAD_ADDRESS       := $034E ; $034F
SENDUDP_SEQUENCE        := $034E ; may be the same as TFTP_LOAD_ADDRESS
RANDOM16                := $0350 ; $0351
FUNCTION_RECALL         := $0352 ; $0353
TIMESTAMP_RECALL        := $0354

CS8900A_REVISION        := $0355 ; $00=rev B, $01=rev C, $02=rev D, $03=rev F

TRANSFER_MODE           := $0356
TX_COUNTER              := $0357
RX_COUNTER              := $0358
DROP_COUNTER            := $0359

LAST_KEYCODE            := $035A

.if USE_CHAMELEON_DETECTION
IS_CHAMELEON            := $037F ; $00 = original C64
.endif

FILENAME_BUFFER         := $0380 ; $03FE
FILENAME_CHECKSUM       := $03FF

DHCP_STATE_OFF          := $00
DHCP_STATE_DISCOVER     := $01
DHCP_STATE_OFFER        := $02
DHCP_STATE_REQUEST      := $03
DHCP_STATE_ACK          := $04

TRANSFER_MODE_NONE      := $00
TRANSFER_MODE_TFTP      := $01
TRANSFER_MODE_CODENET   := $02
TRANSFER_MODE_SENDUDP   := $03

; data vectors are pointing to must be 16-bit aligned!

.macro txtout address
   lda   #<address
   ldy   #>address
   jsr   TXTOUT
.endmacro

.macro memcpy7 start,end,dest
   ldx   #<(end-start-1)
:
   lda   start,x
   sta   dest,x
   dex
   bpl   :-
.endmacro

.include "globals.inc"
