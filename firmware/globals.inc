
; (c)opyright 2014-2017
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

; codenet.s
.global     handle_udp_codenet
.global     codenet_send_ack

; cs8900a.s
.global     cs8900a_init
.global     cs8900a_send_ip_frame_16
.global     cs8900a_receive_ip_frame_16
.global     cs8900a_check_hw
.global     cs8900a_setup_mac
.global     cs8900a_get_packet_page
.global     cs8900a_print_dropped
.global     cs8900a_print_tx
.global     cs8900a_print_rx

; echo.s
.global     handle_echo_package

; tftp.s
.global     handle_tftp_data
.global     handle_tftp_command
.global     tftp_request_file
.global     tftp_replace_filename

; lib.s
.global     jt_print_hexbyte
.global     jt_convertascii
.global     jt_print
.global     jt_printascii
.global     jt_tftp_load_from_basic

.global     advance_area_start
.global     advance_area_start_a
.global     advance_area_start_by2
.global     advance_temp_a_1
.global     area_align_end_16
.global     area_check_end_16
.global     area_get_size
.global     area_set_end
.global     area_set_end_udp
.global     area_set_start
.global     char_out
.global     clear_output
.global     clear_netbuffer
.global     petscii_to_ascii
.global     print
.global     printascii
.global     print_dec
.global     hexbyte_out

; server.s
.global     run_server_enter_ip
.global     run_server_static_ip
.global     run_server_dhcp
.global     recall_after_2s
.global     recall_after_2m
.global     recall_cancel

; udpslave.s
.global     handle_udp_sendudp

; dhcp.s
.global     set_dhcp_state
.global     print_dhcp_state
.global     dhcp_create_package_discover
.global     handle_dhcp_package

; arp.s
.global     reply_arp

; rrnetmk3.s
.global     calculate_filename_checksum
.global     calculate_ip_header_checksum
.global     calculate_udp_checksum
.global     check_keys_pressed
.global     current_address_out
.global     disable_and_basic
.global     disable_and_jump
.global     disable_and_run
.global     enter_ip_address
.global     package_ether_reply
.global     package_ip_reply
.global     print_hw_info
.global     print_status
.global     receive_loop
.global     replace_tftp_filename
.global     restart_server
.global     save_remote_ip_and_mac
.global     set_ip_header_checksum
.global     set_random16
.global     set_udp_checksum
.global     tftp_load_from_basic
.global     transfer_ip_reply
.global     udp_send_package
.global     udp_set_package_sizes
.global     ROM_MAC_ADDR
