CHACOCMD := ~/src/6502/chameleon/Chameleon_Beta-9c/ChaCo_Linux64/chacocmd 
PROGRAM := $(shell basename $(CURDIR)).bin
SYMBOLS := $(shell basename $(CURDIR)).sym
TYPE := cbm80
SOURCES := .
INCLUDES := .
LIBS :=
OBJDIR := obj
DEBUGDIR := $(OBJDIR)
 
LINKCFG := c64-$(TYPE).ld
ASFLAGS := --cpu 6502x
LDFLAGS	= -C$(LINKCFG) \
          -m $(DEBUGDIR)/$(notdir $(basename $@)).map \
          -Ln $(DEBUGDIR)/$(notdir $(basename $@)).labels -vm
 
################################################################################
 
LD            := ld65
AS            := ca65
OD            := od65
 
MKDIR         := mkdir
RM            := rm -f
RMDIR         := rm -rf
 
################################################################################
 
ofiles :=
sfiles := $(foreach dir,$(SOURCES),$(sort $(notdir $(wildcard $(dir)/*.s))))
incfiles := $(foreach dir,$(INCLUDES),$(notdir $(wildcard $(dir)/*.inc)))
extra_includes := $(foreach i, $(INCLUDES), -I $i)
 
define depend
  my_obj := $$(addprefix $$(OBJDIR)/, $$(addsuffix .o65, $$(notdir $$(basename $(1)))))
  ofiles += $$(my_obj)
 
  $$(my_obj): $(1) $(incfiles) Makefile
	$$(AS) -g -o $$@ $$(ASFLAGS) $(extra_includes) $$<
endef
 
################################################################################
 
.SUFFIXES:
.PHONY: all clean run rundebug
all: $(PROGRAM)

slot1: $(PROGRAM)
	$(CHACOCMD) --addr 0x00110000 --writemem $<
slot2: $(PROGRAM)
	$(CHACOCMD) --addr 0x00120000 --writemem $<
slot3: $(PROGRAM)
	$(CHACOCMD) --addr 0x00a00000 --writemem $<
slot4: $(PROGRAM)
	$(CHACOCMD) --addr 0x00b00000 --writemem $<
ram: $(PROGRAM)
	$(CHACOCMD) --addr 0x00008000 --writemem $<
 
$(foreach file,$(sfiles),$(eval $(call depend,$(file))))
 
$(OBJDIR):
	[ -d $@ ] || mkdir -p $@
 
$(PROGRAM): $(OBJDIR) $(ofiles) ../svnversion.inc
	$(LD) -o $@ $(LDFLAGS) $(ofiles) $(LIBS)
	
$(SYMBOLS): $(PROGRAM)
	cat $(DEBUGDIR)/$(shell basename $(CURDIR)).labels | awk '{printf("%s %s\n", $$3, $$2)}' \
		> $(SYMBOLS)
 
run: $(PROGRAM)
	$(EMULATOR) $(EMULATORFLAGS) $(PROGRAM)
 
rundebug go: $(SYMBOLS)
	$(EMULATOR) -debug $(EMULATORFLAGS) $(PROGRAM)
 
clean:
	$(RM) $(ofiles) $(PROGRAM) $(SYMBOLS)
	$(RMDIR) $(OBJDIR)

stat: $(PROGRAM)
	$(OD) -S $(ofiles)|grep -v -e ' 0$$' -e 'Segment sizes:'|tee $(DEBUGDIR)/stat.txt
	@grep -e RODATA: -e CODE: -e VECTORS: ${DEBUGDIR}/stat.txt | awk '{SUM += $$2} END {print "Total:\n                               " SUM " (" 8192-SUM " left)"}'


