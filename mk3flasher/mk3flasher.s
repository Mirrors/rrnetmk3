
; (c)opyright 2015-2018
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

.include "common.i"
.include "../svnversion.inc"

.define USE_CRC16    0

ST             := $90
DEVADR         := $BA
IOBVEC         := $AE
DISCUR         := $CC
TEMP_A_16      := $FB ; $FC

CURRENT_LINE   := $D1
CURRENT_COLUMN := $D3

RRCTRL         := $DE00
RREXTD         := $DE01
ROM_ENABLE     := $DE80
ROM_DISABLE    := $DE88

ERRMSG         := $A43A
VALFNC         := $B7B5
FL2INT         := $BC9B
AXOUT          := $BDCD
READST         := $FFB7
SETLFS         := $FFBA
SETNAM         := $FFBD
OPEN           := $FFC0
CLOSE          := $FFC3
CHKIN          := $FFC6
CHKOUT         := $FFC9
CLRCHN         := $FFCC
BASIN          := $FFCF
BSOUT          := $FFD2
GETIN          := $FFE4
PLOT           := $FFF0

IP_ON_SCREEN   := $04B6
MAC_ON_SCREEN  := $0506

FW_MAC_ADDR    := $9FF8
FW_CHKSUM0     := $9FFE
FW_CHKSUM1     := $9FFF

.segment "RODATA"

spinner:
   .byte $40,$4d,$5d,$4e

.segment "CODE"
        .export Start
Start:
   jmp   main

; crc-16 calculation taken from
; http://www.6502.org/source/integers/crc-more.html

.if USE_CRC16
calc_crc16:
   ldy   #$ff
   sty   TEMP_A_16+0
   sty   TEMP_A_16+1
   iny
   lda   #$80
   sty   IOBVEC+0
   sta   IOBVEC+1
:
   lda   (IOBVEC),y
   eor   TEMP_A_16+1 ; A contained the data
   sta   TEMP_A_16+1 ; XOR it into high byte
   lsr               ; right shift A 4 bits
   lsr               ; to make top of x^12 term
   lsr               ; ($1...)
   lsr
   tax               ; save it
   asl               ; then make top of x^5 term
   eor   TEMP_A_16+0 ; and XOR that with low byte
   sta   TEMP_A_16+0 ; and save
   txa               ; restore partial term
   eor   TEMP_A_16+1 ; and update high byte
   sta   TEMP_A_16+1 ; and save
   asl               ; left shift three
   asl               ; the rest of the terms
   asl               ; have feedback from x^12
   tax               ; save bottom of x^12
   asl               ; left shift two more
   asl               ; watch the carry flag
   eor   TEMP_A_16+1 ; bottom of x^5 ($..2.)
   sta   TEMP_A_16+1 ; save high byte
   txa               ; fetch temp value
   rol               ; bottom of x^12, middle of x^5!
   eor   TEMP_A_16+0 ; finally update low byte
   ldx   TEMP_A_16+1 ; then swap high and low bytes
   sta   TEMP_A_16+1
   stx   TEMP_A_16+0
   iny
   bne   :-
   inc   IOBVEC+1
   lda   IOBVEC+1
   cmp   #$a0
   bne   :-
   rts
.endif

read_file:
   lda   #$01
   ldx   DEVADR
   bne   :+
   ldx   #$08
:
   ldy   #$00
   jsr   SETLFS
   lda   #<(filename_end-filename)
   ldx   #<filename
   ldy   #>filename
   jsr   SETNAM
   jsr   OPEN
   bcc   :+
   jmp   ERRMSG+1 ; skip the TAX
:
   ldx   #$01
   jsr   CHKIN
   
   lda   #$00
   sta   IOBVEC+0
   lda   #$80
   sta   IOBVEC+1
   
@readloop:
   jsr   BASIN
   ldy   #$00
   sta   (IOBVEC),y
   bit   ST
   bvs   @eof
   ldy   IOBVEC+1
   cpy   #$a0
   beq   @eof
   inc   IOBVEC+0
   bne   @readloop
   inc   IOBVEC+1
   lda   IOBVEC+1
   and   #$03
   tay
   lda   spinner,y
   ldy   CURRENT_COLUMN
   sta   ($D1),y
   jmp   @readloop
@eof:
   lda   #$01
   jsr   CLOSE
   jsr   CLRCHN
   lda   #$20
   jsr   BSOUT
   lda   #$0d
   jsr   BSOUT

   lda   IOBVEC+0
   cmp   #$ff
   bne   @wrongsize
   lda   IOBVEC+1
   cmp   #$9f
   bne   @wrongsize
   
   rts
@wrongsize:
   jsr   print
   .byte "file is not exactly 8192 bytes in size!",$0d,$00
   pla
   pla
   jmp   ($a002)

diskstatus:
   lda   #$01
   ldx   DEVADR
   bne   :+
   ldx   #$08
:
   ldy   #$0F
   jsr   SETLFS
   lda   #$00
   jsr   SETNAM
   jsr   OPEN
   bcs   @error
   ldx   #$01
   jsr   CHKIN
@loop:
   jsr   READST
   bne   @done
   jsr   BASIN
   jsr   BSOUT
   jmp   @loop
@error:
   ; TODO
@done:
   lda   #$01
   jsr   CLOSE
   jmp   CLRCHN

main:
   jsr   $E518

   jsr   print
   .byte $93,$8e
   .byte $02,$01,$01,"* rr-net-mk3 flasher (rev. "
   .byte SVNREVISION
   .byte ") *",$0d,$0d,$00

   jsr   print
   .byte "reading "
filename:
   .byte "rrnetmk3.rom"
filename_end:
   .byte $20,$00
   jsr   read_file
   jsr   diskstatus

.if USE_CRC16
   jsr   print
   .byte $0d,"crc16:",$00

   jsr   calc_crc16
   lda   TEMP_A_16+1
   jsr   hexbyte_out
   lda   TEMP_A_16+0
   jsr   hexbyte_out
.endif
   lda   #$0d
   jsr   BSOUT

   sei
   jmp   flash_and_verify
