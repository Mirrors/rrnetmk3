
; (c)opyright 2015-2017
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

.include "common.i"

ST             := $90
DEVADR         := $BA
IOBVEC         := $AE
DISCUR         := $CC
TEMP_A_16      := $FB ; $FC

CURRENT_LINE   := $D1
CURRENT_COLUMN := $D3

RRCTRL         := $DE00
RREXTD         := $DE01
ROM_ENABLE     := $DE80
ROM_DISABLE    := $DE88

ERRMSG         := $A43A
VALFNC         := $B7B5
FL2INT         := $BC9B
AXOUT          := $BDCD
READST         := $FFB7
SETLFS         := $FFBA
SETNAM         := $FFBD
OPEN           := $FFC0
CLOSE          := $FFC3
CHKIN          := $FFC6
CHKOUT         := $FFC9
CLRCHN         := $FFCC
BASIN          := $FFCF
BSOUT          := $FFD2
GETIN          := $FFE4
PLOT           := $FFF0

IP_ON_SCREEN   := $04B6
MAC_ON_SCREEN  := $0506

ROM_MAC_ADDR   := $9FF8
ROM_CHKSUM0    := $9FFE
ROM_CHKSUM1    := $9FFF

.segment "RODATA"

FIRMWARE_START:
.incbin "original.bin"
FIRMWARE_END:
FW_MAC_ADDR := FIRMWARE_START + $1FF8
FW_CHKSUM0  := FIRMWARE_START + $1FFE
FW_CHKSUM1  := FIRMWARE_START + $1FFF

.segment "CODE"
        .export Start
Start:
   jmp   main

update_chksum:
   jsr   rrnet_chksum
   sta   ROM_CHKSUM0
   stx   ROM_CHKSUM1
   rts

verify_chksum:
   jsr   rrnet_chksum
   cmp   ROM_CHKSUM0
   bne   :+
   cpx   ROM_CHKSUM1
:
   rts

rrnet_chksum:
   lda   ROM_MAC_ADDR+4 ; MAC_LO
   eor   ROM_MAC_ADDR+5 ; MAC_HI ^ MAC_LO
   eor   #$55           ;  ^ $55
   pha                  ; == CHKSUM0

   clc
   adc   ROM_MAC_ADDR+4 ; + MAC_LO
   clc
   adc   ROM_MAC_ADDR+5 ; + MAC_HI
   eor   #$aa           ;  ^ $aa
   tax                  ; == CHKSUM1
   pla
   rts

main:
   jsr   $E518

   sta   ROM_ENABLE
   jsr   verify_chksum
   bne   @novalidmac
   ldy   #$07
:
   lda   ROM_MAC_ADDR,y
   sta   FW_MAC_ADDR,y
   dey
   bpl   :-

@novalidmac:
   sta   ROM_DISABLE

   jsr   print
   .byte $8e
   .byte $02,$01,$01,"* rr-net-mk3 flasher (original) *"
   .byte $02,$03,$01,"mac address: ",$00

   ldy   #$00
:
   lda   FW_MAC_ADDR,y
   jsr   hexbyte_out
   iny
   cpy   #$06
   bcs   :+
   lda   #':'
   jsr   BSOUT
   jmp   :-
:

   jsr   input_mac

   jsr   print
   .byte $0d,$0d,$00

   sei

   jsr   upload_firmware
   jsr   update_chksum
   jmp   flash_and_verify

upload_firmware:
   lda   #<FIRMWARE_START
   sta   @src+1
   lda   #>FIRMWARE_START
   sta   @src+2

   ldy   #$00
   sty   @dest+1
   lda   #$80
   sta   @dest+2

   ldx   #$20
@src:
   lda   FIRMWARE_START,y
@dest:
   sta   $8000,y
   iny
   bne   @src
   inc   @src+2
   inc   @dest+2
   dex
   bne   @src
   rts

print_mac_address:
   ldy   #$00
:
   lda   FW_MAC_ADDR,y
   jsr   hexbyte_out
   iny
   cpy   #$06
   bcs   :+
   lda   #':'
   jsr   BSOUT
   jmp   :-
:
   rts

input_mac:
   ldy   #$1A
   sty   CURRENT_COLUMN
   lda   #$00
   sta   DISCUR
@loop:
   jsr   GETIN
   beq   @loop
   sta   IOBVEC
   ldy   CURRENT_COLUMN
   lda   (CURRENT_LINE),y
   and   #$7f
   sta   (CURRENT_LINE),y
   lda   IOBVEC
   cmp   #$0d
   beq   @done
   cmp   #$14 ; backspace
   bne   :+
   lda   #$9d ; same as crsr<-
   sta   IOBVEC
:
   cmp   #$1d ; crsr->
   beq   @ok
   cmp   #$9d ; crsr<-
   beq   @ok
   cmp   #'a'
   bcc   :+
   cmp   #'f'+1
   bcc   @ok
:
   cmp   #'0'
   bcc   @loop
   cmp   #'9'+1
   bcs   @loop
@ok:
   jsr   BSOUT

   ldy   CURRENT_COLUMN
   cpy   #$1A
   bcs   :+
   ldy   #$1A
:
   cpy   #$1E
   bcc   :+
   ldy   #$1E
:
   sty   CURRENT_COLUMN
   lda   (CURRENT_LINE),y
   cmp   #':'
   bne   @loop
   lda   IOBVEC
   cmp   #$9d
   beq   :+
   lda   #$1d
:
   jsr   BSOUT
   jmp   @loop
@done:
   lda   #$01
   sta   DISCUR

   ldx   #$00

   ldy   #$0E
@hexloop:
   lda   (CURRENT_LINE),y
   iny
   jsr   screentohexnibble
   asl
   asl
   asl
   asl
   sta   IOBVEC
   lda   (CURRENT_LINE),y
   iny
   iny
   jsr   screentohexnibble
   ora   IOBVEC
   sta   FW_MAC_ADDR,x
   inx
   cpx   #$06
   bcc   @hexloop
   rts

screentohexnibble:
   cmp   #'0'
   bcc   :+
   and   #$0f
   rts
: ;clc because of cmp
   adc   #$09
   rts
