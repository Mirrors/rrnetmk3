
; (c)opyright 2015-2018
; Sven Oliver ("SvOlli") Moll
; and Individual Computers

.include "common.i"

print:
   pla
   sta   IOBVEC+0
   pla
   sta   IOBVEC+1
@mainloop:
   ldy   #$00
   jsr   advance_temp_a_1
   lda   (IOBVEC),y
   bne   @evaluate
   lda   IOBVEC+1
   pha
   lda   IOBVEC+0
   pha
   rts

@evaluate:
   cmp   #$01           ; CTRL-A number char: print number of chars
   bne   @norle
   jsr   advance_temp_a_1
   lda   (IOBVEC),y
   tax
   jsr   advance_temp_a_1
   lda   (IOBVEC),y
@loopout:
   jsr   BSOUT
   dex
   bne   @loopout
   beq   @mainloop

@norle:
   cmp   #$02           ; CTRL-B line column: position cursor
   bne   @nopos
   jsr   advance_temp_a_1
   lda   (IOBVEC),y
   tax
   jsr   advance_temp_a_1
   lda   (IOBVEC),y
   tay
   clc
   jsr   PLOT
   jmp   @mainloop
@nopos:
   jsr   BSOUT
   jmp   @mainloop   ; always true

hexbyte_out:
   pha
   lsr
   lsr
   lsr
   lsr
   jsr   hexnib_out
   pla
   and   #$0f
hexnib_out:
   ora   #$30
   cmp   #$3a
   bcc   :+
   adc   #$06
:
   jmp   BSOUT

advance_temp_a_1:
   inc   IOBVEC+0
   bne   :+
   inc   IOBVEC+1
:
   rts

flash_and_verify:
   jsr   print
   .byte $91,$0d,"writing to eeprom ",$00

   ldx   #$00
   stx   TEMP_A_16+0
   lda   #$80
   sta   TEMP_A_16+1

@copyloop:
   lda   TEMP_A_16+1
   sta   @addr1+1
   sta   @addr2+1
   jsr   hexbyte_out
   lda   TEMP_A_16+0
   sta   @addr1+0
   sta   @addr2+0
   jsr   hexbyte_out
   lda   CURRENT_COLUMN
   sec
   sbc   #$04
   sta   CURRENT_COLUMN
   sei
   ldy   #$00
   lda   (TEMP_A_16),y
   sta   ROM_ENABLE
:
@addr1 = * + 1
   sta   $dead
   ;sta   (TEMP_A_16),y
@addr2 = * + 1
   cmp   $beef
   ;cmp   (TEMP_A_16),y
   bne   :-
   sta   ROM_DISABLE
   inc   TEMP_A_16+0
   bne   @copyloop
   inc   TEMP_A_16+1
   lda   TEMP_A_16+1
   cmp   #$a0
   bne   @copyloop

   lda   #$0d
   jsr   BSOUT

; for debugging
.if 0
   lda   $8888
   eor   #$88
   sta   $8888
.endif

   sei
   lda   #$80
   sta   TEMP_A_16+1

@verifyloop:
   jsr   print
   .byte $91,$0d,"verifying eeprom ",$00

   lda   TEMP_A_16+1
   jsr   hexbyte_out
   lda   TEMP_A_16+0
   jsr   hexbyte_out
   lda   CURRENT_COLUMN
   sec
   sbc   #$04
   sta   CURRENT_COLUMN
   sei
   ldy   #$00
   lda   (TEMP_A_16),y
   sta   ROM_ENABLE
   cmp   (TEMP_A_16),y
   beq   @equal
   jsr   print
   .byte $91,$0d,"verify error at address ",$00

   lda   TEMP_A_16+1
   jsr   hexbyte_out
   lda   TEMP_A_16+0
   jsr   hexbyte_out
   lda   #$0d
   jsr   BSOUT

@equal:
   sta   ROM_DISABLE
   inc   TEMP_A_16+0
   bne   @verifyloop
   inc   TEMP_A_16+1
   lda   TEMP_A_16+1
   cmp   #$a0
   bne   @verifyloop

   lda   #$0d
   jsr   BSOUT

   jmp   ($a002)
