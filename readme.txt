--------------------------------------------------------------------------------
Using the RRNET firmware patcher tool
--------------------------------------------------------------------------------

This tool will generate a new updated firmware that can be used with an RR-Net
equipped Retro Replay, MMC Replay, MMC64, Chameleon or RR-Net MK3.

* Turn on your C64. When using the RR-Net MK3 make sure it is active, because
  the tool will read its MAC address from it.
* LOAD and RUN "rrnet-tool". You will then be prompted to enter a default MAC
  address and IP address.
* The text after "DC0E" and "9FFC" show a hexdump of four bytes at that
  address for easier spotting of possible problems.
* After confirming with RETURN you will be prompted to save either a plain
  binary or a .crt file. (for flashing to RR-Net MK3, Retro Replay, etc select
  plain binary)
* Insert a disk with at least 33 blocks free (130 if you are saving a retro
  replay crt file) and then make your selection, and the patched firmware will
  be saved to disk.

--------------------------------------------------------------------------------
Installing the generated firmware
--------------------------------------------------------------------------------

RR-Net MK3:
-----------

WARNING: Originally the RR-Net MK3 was not designed to be (re)flashed by end-
         users - because of that flashing will only work correctly on "new" C64
         boards (ASSY 250469 / "ALDI" "short" board).

         If you don't understand what that means and what the risks are, PLEASE
         DO NOT ATTEMPT TO REFLASH YOUR RR-NET MK3.

* Close the jumper to enable flashing mode.
* Plug in the RR-Net MK3 and start the C64. The RR-Net startup message will NOT
  be shown.
* Load and run the tool "rrnetmk3flasher". It will then load the previously
  generated file and write it into the eeprom.
* Once it's done power down the C64 and open the jumper again.

Chameleon:
----------

Start the generated .crt file from within the file browser. If you want to
install the firmware permanently, use the update.prg as described in the user
manual.

Retro Replay:
-------------

Use the regular flash tool and flash the generated firmware the same way you
would install a reguler Retro Replay ROM.

MMC Replay:
-----------

Install the firmware in a cartridge slot of your choice as described in the
user manual. The cartridge emulation type should be set to "Retro Replay"

MMC64:
------

== TODO ==

--------------------------------------------------------------------------------
Using the firmware
--------------------------------------------------------------------------------

Like the original RRNET-MK3 firmware, this firmware will start into basic, with
a slightly modified power-up message.

hold CTRL at reset to go the help/instructions menu
hold C= at reset to start the servers with manual IPv4 setup
hold left SHIFT at reset to start the servers with DHCPv4
(DHCPv4 is a non-complete implementation, the renewal of an address is fixed to
about two minutes. Under normal usage of the cartridge, this should not be a
problem. It might be not wise to let the servers "idle" for hours, though.)

currently the following servers for recieving data/programs are implemented:

- port 6462: codenet (by graham)
- port 3172: udp slave (by magervalp)
- port 69:   tftp (rfc1350), put will load a program to ram

For testing purposes and as an example also port 7 (echo) is implemented.

After BASIC has been started the following information and routines are
available:
- $0334-$033f (820-831): copy of $9ff4-$9fff of firmware, in detail:
- $0334-$0337 (820-823): ip address (default, or changed via DHCP/input)
- $0338-$033d (824-829): mac address
- $033e-$033f (830-831): mac checksum

SYS333,"filename":
- reset to firmware, start dhcp, replace pxe-filename with "filename" parameter

--------------------------------------------------------------------------------
Build instructions
--------------------------------------------------------------------------------

check out the source repository using SVN:

$ mkdir rrnetmk3
$ cd rrnetmk3
$ svn checkout https://svn.icomp.de/svn/rrnetmk3/trunk/ .

To build the source you will need:

- ca65 from the CC65 toolchain, available from https://github.com/cc65/cc65
- GNU make
- exomizer (optional)
- c1541 (from the VICE package) for generating the d64 file
- zip (to generate the release package)

To build the firmware and -patching tool, just type "make" in the root of the
source tree.

$ make

For testing in VICE use a recent nightly build (it will not work in 2.4!) - have
a look at the Makefile for various options/configurations.

We are happy to merge your improvements and fixes, please send them to
tobias@icomp.de

--------------------------------------------------------------------------------
Notes
--------------------------------------------------------------------------------

If you want to implement your own udp based service, search for "echo" in the
source code. It implements a service that is just echoing back each packet it
gets on port 7. It should provide a good basis for your ideas.

Another hint: Beware of ghost reads before writes in the IO-area!
sta $de0x,x is a bad idea, as it was learned the hard way...
